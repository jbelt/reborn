﻿using UnityEngine;
using System.Collections;

public class CombineTongs : DualIScript {

	public Interactable newItem;
	public string targetName;
	private GameObject disabledArea;
	protected override void OnSuccess(Interactable other)
	{
		if (other.name == "Rotting Head")
		{
			GameObject target = Helpers.GetGameObject(targetName);
			Helpers.Inventory.RemoveInteractable(Interactable, target);
			Helpers.Inventory.AddInteractable(newItem);
			//Helpers.Teleport(target, disabledArea);

		}
	}
	// Update is called once per frame
	protected override void OnOutOfRange(Interactable other)
	{
	}
}
