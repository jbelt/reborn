﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	public int boundary = 50;
	public int speed = 10;
	private int screenWidth;
	private int screenHeight;
	public int screenMaxLeft;
	public int screenMaxRight;

	void Start () {
		screenWidth = Screen.width;
		screenHeight = Screen.height;
	}
	void Update() 
	{
		Vector3 newpos = transform.position;
//		Debug.Log(newpos.x);
			if (Input.mousePosition.x > screenWidth - boundary)
			{
				newpos.x += speed * Time.deltaTime;
				transform.position = newpos;
			//	Debug.Log(newpos.x);
			}
	

			if (Input.mousePosition.x < 0 + boundary)
			{
				newpos.x -= speed * Time.deltaTime;
				transform.position = newpos;
			}
	} 

}
