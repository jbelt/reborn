﻿using UnityEngine;
using System.Collections.Generic;

public class CombineItems : DualIScript
{
	public string otherName;
	
	protected override void OnSuccess(Interactable other)
	{
		if (other.name == otherName)
		{
			Interactable copy = Helpers.InstantiateInteractable(other);
			Helpers.Inventory.AddInteractable(copy);
		}
	}
	
	protected override void OnOutOfRange(Interactable other)
	{
	}
}
