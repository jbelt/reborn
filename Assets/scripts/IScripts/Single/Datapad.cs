﻿using UnityEngine;
using System.Collections;

public class Datapad : SingleIScript {

	public GUISkin projectSkin;
	static public bool visible = false;

	protected override void OnSuccess()
	{
		visible = true;
	}
	
	protected override void OnOutOfRange()
	{
		
	}
	
	void OnGUI(){
		if(visible)
		{
			GUI.skin = projectSkin;
			GUI.Box(new Rect(Screen.height / 2,0,Screen.height, Screen.height), "");
		}
	}
}
