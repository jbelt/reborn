﻿using UnityEngine;
using System.Collections;

public class KeypadInteract : SingleIScript {

	new int keypadWidth = Screen.width;
	new int keypadHeight = Screen.height;

	public GUISkin projectSkin;
	static public bool visible = false;

	public GUIStyle newstyle;

	protected override void OnSuccess()
	{
		visible = true;
	}

	protected override void OnOutOfRange()
	{

	}

	void OnGUI(){
		if(visible)
		{
			GUI.skin = projectSkin;
			GUI.Box(new Rect(Screen.height / 2,0,Screen.height / 1.4f, Screen.height), "");
		}
	}
}
