﻿using UnityEngine;
using System.Collections;

public class Look : SingleIScript {
	public string[] dialogue;
	
	protected override void OnSuccess()
	{
		Helpers.DialogueManager.SpeakNow(dialogue);
	}
	
	protected override void OnOutOfRange()
	{
	}
}
