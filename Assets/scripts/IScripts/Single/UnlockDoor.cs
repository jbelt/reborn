﻿using UnityEngine;
using System.Collections;

public class UnlockDoor : SingleIScript {
	public string[] dialogue;
	
	protected override void OnSuccess()
	{
		Helpers.DialogueManager.SpeakNow(dialogue);
		Helpers.Game.SetFlag("doorLocked", false);
	}
	
	protected override void OnOutOfRange()
	{
	}
}
