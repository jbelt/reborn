﻿using UnityEngine;
using System.Collections;

public class Pickup : SingleIScript
{
	protected override void OnSuccess()
	{
		Helpers.Inventory.AddInteractable(Interactable);
	}
	
	protected override void OnOutOfRange()
	{
	}
}

