﻿using UnityEngine;
using System.Collections;

public class EnterDoor : SingleIScript {
	public string sceneName;
	public string waypointName;

	protected override void OnSuccess()
	{
		Helpers.TeleportPlayerInOtherScene(waypointName,sceneName);
	}

	protected override void OnOutOfRange()
	{
	}

}
