﻿using UnityEngine;
using System.Collections;

public class CombineFire : DualIScript {

	public string targetName;
	public GameObject removedObject;
	protected override void OnSuccess(Interactable other)
	{
		if (other.name == "Fire")
		{
			GameObject target = Helpers.GetGameObject(targetName);
			Helpers.Inventory.RemoveInteractable(Interactable, target);
			Helpers.Game.RemoveObjectsWithNameFromScene(GameObject.Find("Vines"), true, false);
			                                         
		}
	}
	// Update is called once per frame
	protected override void OnOutOfRange(Interactable other)
	{
	}
}
