﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class ReplacementMovementsScript : MonoBehaviour {

	#region Modes
	
	
	public enum MoveMode
	{
		None,
		Move,
	}
	MoveMode mode = MoveMode.None;
	public MoveMode Mode
	{
		get { return mode; }
	}
	
	/// <summary>
	/// What player should do when finishing walking.
	/// </summary>
	enum MoveAction
	{
		None,
		SingleInteraction,
		DualInteraction,
	}
	MoveAction moveAction = MoveAction.None;
	
	
	#endregion
	
	
	#region Inspector variables
	
	
	public float moveTime = 5f;
	
	
	#endregion
	
	
	#region Fields
	
	
	Seeker seeker;
	
	Path path;
	int currentWaypoint = 0;
	float nextWaypointDistance = 0.5f;
	
	
	#endregion
	
	
	#region Unity event handlers
	
	
	void Start()
	{
		seeker = GetComponent<Seeker>();
		seeker.pathCallback += TurnStart;
	}
	
	void Update()
	{		
		if ((path == null) || (mode == MoveMode.None))
			return;
		
		if (mode == MoveMode.Move)
			MoveUpdate();
		
		if (currentWaypoint >= path.vectorPath.Length)
		{
			MoveFinish();
		}
	}
	
	
	#endregion
	
	
	#region Turning
	
	
	void TurnStart(Path p)
	{
		if (p.error)
			return;

		path = p;
	}
	
	
	#endregion
	
	
	#region Moving
	
	
	void MoveStart()
	{
		mode = MoveMode.Move;
	}
	
	void MoveUpdate()
	{
		// update position
		transform.position = Vector3.MoveTowards(
			transform.position, path.vectorPath[currentWaypoint],
			moveTime / 100);
		
		// update rotation (smoothly)		
		iTween.LookUpdate(gameObject, 
		                  iTween.Hash("looktarget",path.vectorPath[currentWaypoint], 
		            "time",10f, "axis","y"));
		
		// check for waypoint change
		float distToWaypoint = Vector3.Distance(transform.position, 
		                                        path.vectorPath[currentWaypoint]);
		
		if (distToWaypoint < nextWaypointDistance)
			currentWaypoint++;
	}
	
	void MoveFinish()
	{
		currentWaypoint = 0;
		mode = MoveMode.None;
		
		switch (moveAction)
		{
		case MoveAction.SingleInteraction:
			Player.Instance.ExecuteCurrentScript();	
			break;
		case MoveAction.DualInteraction:
			Player.Instance.ExecuteDualInteractionScript();
			break;
		}
		
		moveAction = MoveAction.None;
	}
	
	/// <summary>
	/// Moves player to a point.
	/// </summary>
	public void Move(Vector3 point)
	{
		Helpers.UpdateNavMesh();
		
		// start calculating path
		seeker.StartPath(transform.position, point);
		moveAction = MoveAction.None;
	}
	
	/// <summary>
	/// Moves player to point and attempts to trigger single interaction when finished.
	/// </summary>
	public void MoveAndSingleInteract(Vector3 point, InteractionScript iScript)
	{
		Move(point);
		
		moveAction = MoveAction.SingleInteraction;
		Player.Instance.ScriptToExecute = iScript;
	}
	
	/// <summary>
	/// Moves player to point and attempts to trigger dual interaction when finished.
	/// </summary>
	public void MoveAndDualInteract(Vector3 point)
	{
		Move(point);
		
		moveAction = MoveAction.DualInteraction;
	}
	
	
	#endregion
}
