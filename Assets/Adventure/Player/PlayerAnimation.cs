using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour
{
	void Update()
	{
		switch (Player.Instance.Movement.Mode)
		{
			case PlayerMovement.MoveMode.None:
				animation.CrossFade("idle");
				break;
/*			case PlayerMovement.MoveMode.Turn:
				animation.CrossFade("walk");
				break; */
			case PlayerMovement.MoveMode.Move:
				animation.CrossFade("walk");
				break;
		}
	}
}
