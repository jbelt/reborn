﻿using UnityEngine;
using System.Collections;

public class Cutscene : MonoBehaviour {

	[SerializeField]GUISkin projectskin;
	[SerializeField]int currentPicNum;
	[SerializeField]int picNum;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		GUI.skin = projectskin;
		if(currentPicNum < picNum)
		{
			if(GUI.Button(new Rect(0,0,Screen.width,Screen.height),"")){
				transform.Translate(0,0,1);
				currentPicNum ++;
			}
		}
		else
		{
			if(GUI.Button(new Rect(0,0,Screen.width,Screen.height),"")){
				Application.LoadLevel(2);
			}
		}
	}
}
